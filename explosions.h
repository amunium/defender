// explosions.h

class Explosion {
public:
	Explosion(int x, int y, int magnitude) {
		this->x = x;
		this->y = y;
		this->magnitude = magnitude;
		
		this->radius = 0;
		this->r = 255;
		this->g = 255;
		
		this->frame();
	}
	
	int x;
	int y;
	int magnitude;
	
	int radius;
	int r;
	int g;
	
	bool frame() {
		if (this->radius < this->magnitude * 4) {
			
			this->radius += 4;
			
			int deltar = int(255 / (this->magnitude * 1.5));
			int deltag = int(255 / this->magnitude);
			
			if (this->r >= deltar)
				this->r -= deltar;
			if (this->g >= deltag)
				this->g -= deltag;
			
			return true;
		}
		return false;
	}
};
