#include <allegro.h>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <math.h>
#include <vector>
using namespace std;	// to avoid writing std::vector every time

#include "mathex.h"
#include "guntype.h"
#include "projectiles.h"
#include "enemies.h"
#include "explosions.h"


#define WIDTH		800
#define HEIGHT 		600
#define BASEHEIGHT	40
#define BASEWIDTH	25

#define TICKS_PER_SECOND	50



void init();
void deinit();
bool hittest(int, int, int, int, int, int);

volatile int fps = 0;



// bitmaps

BITMAP *dbuf;
//BITMAP *temp_img;


// sounds

SAMPLE *snd_mg;
SAMPLE *snd_ric;
SAMPLE *snd_expl;

SAMPLE *snd_secondary[4];


// vars

float tower_angle = 0;

int player_hitpoints = 100;

float player_mgheat = 0;		// heat of machinegun (can't fire when it gets over 100)
float player_mgheatd = .15;		// how many units of heat to decrease per tick?
float player_mgheatu = 2;		// how many units of heat to increase per fire?
bool player_mgheated = false;	// for heating. when heat gets over 100, this is true until it reaches 75

int player_secondary = 1;		// secondary weapon selected


int enemy_delay = 100;		// delay before next enemy is spawned in ticks (initially 2 secs)


bool game_end = false;		// should game exit the loop, first chance it gets?
bool game_running = true;	// game is NOT paused or in menu. should start as false later
int game_level = 1;			// the current level
int game_score = 0;



// vector for all projectiles
vector<Bullet> v_bullets;

// vector for all enemies
vector<Enemy> v_enemies;

// vector of all explosions
vector<Explosion> v_explosions;


// tick counter - currently 50 per second
void frame_proc(void) {
	// decrement all recoil counters
	for (int i = 0; i < 4; i++) {
		if (guns[i]->counter > 0)
			guns[i]->counter--;
	}
	
	enemy_delay--;
	
	if (player_mgheat > 0)
		player_mgheat -= player_mgheatd;
}


int main() {
	init();
	
	LOCK_VARIABLE(fps);
	install_int_ex(frame_proc, BPS_TO_TIMER(TICKS_PER_SECOND));
	
	
	init_guns();
	init_enemies();
	
	
	dbuf = create_bitmap(WIDTH, HEIGHT);
	
	
	snd_mg = load_wav("data/sounds/mg.wav");
	snd_ric = load_wav("data/sounds/ric.wav");
	snd_expl = load_wav("data/sounds/expl.wav");
	
	snd_secondary[0] = NULL;
	snd_secondary[1] = load_wav("data/sounds/sabot.wav");
	snd_secondary[2] = load_wav("data/sounds/rocket.wav");
	snd_secondary[3] = load_wav("data/sounds/rail.wav");
	
	
	// offsets for the base. no need to redraw every frame, unless it becomes variable later
	int baseoffset_x = (WIDTH-BASEWIDTH)/2;
	int baseoffset_y = (HEIGHT-BASEHEIGHT)/2;
	
	// offsets for tower
	int center_x = WIDTH/2;
	int center_y = HEIGHT/2;

	
	// main loop
	while (!key[KEY_ESC] && !game_end) {
		vsync();
		clear_bitmap (dbuf);
		
		int i;	// for all level1 loops
		int length;	// for distance calculations
		
		// keys
		if (key[KEY_1])
			player_secondary = 1;
		else if (key[KEY_2])
			player_secondary = 2;
		else if (key[KEY_3])
			player_secondary = 3;
		
		
		// fire machine gun
		if (mouse_b & 1 && guns[0]->counter <= 0) {
			
			// heat calculations
			if (player_mgheated && player_mgheat < 75)	// reset if below 75
				player_mgheated = false;
			if (player_mgheat >= 100) {	// increase heat every fire
				player_mgheated = true;
				player_mgheat = 100;
			}
			
			if (player_mgheat < 100 && !player_mgheated) {
				
				player_mgheat += player_mgheatu;
				
				guns[0]->recoil_reset();		// set back recoil counter
				
				// add a bullet to the vector
				v_bullets.insert(v_bullets.end(), 1, *new Bullet(center_x, center_y, guns[0]->speed, tower_angle, *guns[0] ));
				
				play_sample(snd_mg, 255, 0, 1000, 0);
			}
		}
		
		// secondary weapon firing
		if (mouse_b & 2 && guns[player_secondary]->counter <= 0 && guns[player_secondary]->ammo > 0) {
			
			guns[player_secondary]->recoil_reset();		// set back recoil counter
			guns[player_secondary]->ammo--;
			
			// add a bullet to the vector
			v_bullets.insert(v_bullets.end(), 1, *new Bullet(center_x, center_y, guns[player_secondary]->speed, tower_angle, *guns[player_secondary] ));
			
			play_sample(snd_secondary[player_secondary], 255, 0, 1000, 0);
		}
		
		
		// enemy spawning
		if (enemy_delay < 0) {
			
			int tx = (int)(sin(randexf(0, PI*2)) * 500) + center_x;	// should be 500 + vehicle size
			int ty = (int)(cos(randexf(0, PI*2)) * 500) + center_y;
			v_enemies.insert(v_enemies.end(), 1, *new Enemy(
					tx, ty,
					getangle(center_x, center_y, tx, ty),
					*enemytypes[ randex(0, game_level - 1) ]
				)
			);
			
			// set delay: 0 to 10 seconds minus 10x game level, but not below 1 sec
			enemy_delay = randex(0, 500 - max( game_level*10 , TICKS_PER_SECOND) );
		}
		
		
		/**
		 * DRAWING
		 */
		
		//set_trans_blender(64, 192, 96, 128);
		// draw base (primitively for now)
		//rectfill (dbuf, (WIDTH-BASEWIDTH)/2, (HEIGHT-BASEHEIGHT)/2, WIDTH/2+BASEWIDTH/2, HEIGHT/2+BASEHEIGHT/2, makecol(64, 192, 96));
		rectfill (dbuf, baseoffset_x, baseoffset_y, baseoffset_x+BASEWIDTH, baseoffset_y+BASEHEIGHT, makecol(64, 192, 96));
		
		
		// draw fire
		for (i = 0; i < v_bullets.size(); i++) {
				
				// move the bullet according to angle and speed
				v_bullets.at(i).x += sin(v_bullets.at(i).angle) * v_bullets.at(i).speed;
				v_bullets.at(i).y += cos(v_bullets.at(i).angle) * v_bullets.at(i).speed;
				
				// if the bullet is offscreen, delete it
				if (v_bullets.at(i).x < -100 || v_bullets.at(i).x > WIDTH + 100
				|| v_bullets.at(i).y < -100 || v_bullets.at(i).y > HEIGHT + 100 
				|| v_bullets.at(i).disabled) {
					v_bullets.erase(v_bullets.begin() + i);
					continue;
				}
				
				
				// HIT TEST
				for (int j = 0; j < v_enemies.size(); j++) {
					
					//if (hittest(int(v_bullets.at(i).x), int(v_bullets.at(i).y), int(v_enemies.at(j).x), int(v_enemies.at(j).y), v_enemies.at(j).type->w, v_enemies.at(j).type->h)) {
					for (int k = 0; k < v_bullets.at(i).speed; k += 5) {
						
						int tx = int( sin(v_bullets.at(i).angle + PI) * k + v_bullets.at(i).x );
						int ty = int( cos(v_bullets.at(i).angle + PI) * k + v_bullets.at(i).y );
						
						if (hittest(tx, ty, int(v_enemies.at(j).x - v_enemies.at(j).type->w/2), int(v_enemies.at(j).y - v_enemies.at(j).type->h/2), v_enemies.at(j).type->w, v_enemies.at(j).type->h)) {
							
							v_enemies.at(j).hitpoints -= v_bullets.at(i).type->damage;
							
							if (v_bullets.at(i).type->id > 0 && v_bullets.at(i).type->explosive) {
								v_explosions.insert(v_explosions.end(), 1, *new Explosion(
									int(v_bullets.at(i).x), int(v_bullets.at(i).y), v_bullets.at(i).type->damage/20
								));
							}
							
							if (v_enemies.at(j).hitpoints <= 0) {
								v_explosions.insert(v_explosions.end(), 1, *new Explosion(
									int(v_enemies.at(j).x), int(v_enemies.at(j).y), v_enemies.at(j).type->e_damage
								));
								
								// increase score
								game_score += v_enemies.at(j).type->hitpoints;						
								
								v_enemies.erase(v_enemies.begin() + j);
								play_sample(snd_expl, 255, 0, 1000, 0);
							}
							
							if (!v_bullets.at(i).type->piercing)
								v_bullets.at(i).disabled = true;
							play_sample(snd_ric, 255, 0, 1000, 0);
							
							break;
						}
					}
				}
				
				
				// draw tracers
				drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
				
				// get length of projectile trail to no longer than distance to gun
				length = min( v_bullets.at(i).speed * 10 , (int)sqrt(pow( v_bullets.at(i).x - center_x ,2) + pow( v_bullets.at(i).y - center_y ,2)) - 15 );
				
				// draw machine gun tracer
				if (v_bullets.at(i).type->id == 0) {
					set_screen_blender(0, 0, 0, 64);
					line (dbuf, (int)v_bullets.at(i).x, (int)v_bullets.at(i).y,
						(int)(v_bullets.at(i).x - sin(v_bullets.at(i).angle) * length),
						(int)(v_bullets.at(i).y - cos(v_bullets.at(i).angle) * length),
						makecol(255, 255, 255)
					);
					circlefill (dbuf, (int)v_bullets.at(i).x , (int)v_bullets.at(i).y, 1, makecol(255, 212, 128));
				}
				// draw sabot tracer
				else if (v_bullets.at(i).type->id == 1) {
					set_screen_blender(0, 0, 0, 72);
					line (dbuf, (int)v_bullets.at(i).x, (int)v_bullets.at(i).y,
						(int)(v_bullets.at(i).x - sin(v_bullets.at(i).angle) * length),
						(int)(v_bullets.at(i).y - cos(v_bullets.at(i).angle) * length),
						makecol(255, 255, 0)
					);
					circlefill (dbuf, (int)v_bullets.at(i).x , (int)v_bullets.at(i).y, 2, makecol(255, 255, 0));
				}
				// draw rocket trail
				else if (v_bullets.at(i).type->id == 2) {
					set_screen_blender(0, 0, 0, 72);
					line (dbuf, (int)v_bullets.at(i).x, (int)v_bullets.at(i).y,
						(int)(v_bullets.at(i).x - sin(v_bullets.at(i).angle) * length),
						(int)(v_bullets.at(i).y - cos(v_bullets.at(i).angle) * length),
						makecol(255, 255, 0)
					);
					circlefill (dbuf, (int)v_bullets.at(i).x , (int)v_bullets.at(i).y, 2, makecol(255, 255, 0));
				}
				// draw railgun tracer
				else if (v_bullets.at(i).type->id == 3) {
					set_screen_blender(0, 0, 0, 128);
					line (dbuf, (int)v_bullets.at(i).x, (int)v_bullets.at(i).y,
						(int)(v_bullets.at(i).x - sin(v_bullets.at(i).angle) * length),
						(int)(v_bullets.at(i).y - cos(v_bullets.at(i).angle) * length),
						makecol(32, 64, 255)
					);
					//circlefill (dbuf, (int)v_bullets.at(i).x , (int)v_bullets.at(i).y, 2, makecol(255, 255, 0));
				}
				
				solid_mode();
		}
		
		
		// draw enemies
		for (i = 0; i < v_enemies.size(); i++) {
			
			v_enemies.at(i).x += sin(v_enemies.at(i).angle) * v_enemies.at(i).type->speed;
			v_enemies.at(i).y += cos(v_enemies.at(i).angle) * v_enemies.at(i).type->speed;
			
			length = abs((int)sqrt(pow(v_enemies.at(i).x - center_x, 2) +  pow(v_enemies.at(i).y - center_y, 2)));
			if (length < 30) {
				
				// explode
				player_hitpoints -= v_enemies.at(i).type->e_damage;
				play_sample(snd_expl, 255, 0, 1000, 0);
				
				// insert new explosion
				v_explosions.insert(v_explosions.end(), 1, *new Explosion(
					int(v_enemies.at(i).x), int(v_enemies.at(i).y), v_enemies.at(i).type->e_damage
				));
				
				v_enemies.erase(v_enemies.begin() + i);
				
				
				// PLAYER DEATH
				if (player_hitpoints <= 0) {
					alert("You died, sucker!", "", "", "OK", NULL, 0, 0);
					game_end = true;
				}
				
				continue;
			}
			
			pivot_sprite(dbuf, enemy_bitmaps[v_enemies.at(i).type->imagenum], int(v_enemies.at(i).x), int(v_enemies.at(i).y),
			  v_enemies.at(i).type->w/2, v_enemies.at(i).type->h/2, ftofix(64 - v_enemies.at(i).angle / PI * 128));
			
		}
		
		
		// draw explosions
		for (i = 0; i < v_explosions.size(); i++) {
			for (int j = 0; j < v_explosions[i].magnitude / 2; j++)
				circle (dbuf, v_explosions[i].x, v_explosions[i].y, v_explosions[i].radius + j, makecol( v_explosions[i].r, v_explosions[i].g, 0 ));
			
			if (!v_explosions[i].frame())
				v_explosions.erase(v_explosions.begin() + i);
		}
		
		
		// DEBUG
		/*
		textprintf_ex(dbuf, font, 2, 2, makecol(0, 192, 255), 0, "bullets: %d", v_bullets.size());
		textprintf_ex(dbuf, font, 2, 16, makecol(0, 192, 255), 0, "enemies: %d", v_enemies.size());
		textprintf_ex(dbuf, font, 2, 40, makecol(255, 192, 0), 0, "HP: %d", player_hitpoints);
		textprintf_ex(dbuf, font, 2, 48, makecol(255, 192, 0), 0, "Heat: %f", player_mgheat);
		*/
		// DEBUG END!
		
		// Stat drawing
		drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
		set_screen_blender(0, 0, 0, 128);
		
		// weapons
		rectfill (dbuf, 696, 4, 796, 32, makecol(0, 32, 128));
		
		int color0 = makecol(0, 128, 0);
		int color1 = makecol(255, 128, 0);
		textprintf_right_ex(dbuf, font, 792, 4, player_secondary == 1 ? color1 : color0, -1, "Sabot: %d", guns[1]->ammo);
		textprintf_right_ex(dbuf, font, 792, 14, player_secondary == 2 ? color1 : color0, -1, "Rocket: %d", guns[2]->ammo);
		textprintf_right_ex(dbuf, font, 792, 24, player_secondary == 3 ? color1 : color0, -1, "Railgun: %d", guns[3]->ammo);
		
		// score
		textprintf_ex(dbuf, font, 4, 4, makecol(0, 96, 144), 0, "Score: %d", game_score);
		
		textout_ex(dbuf, font, "Heat", 4, 574, makecol(64,128,0), -1);
		rectfill (dbuf, 100, 574, int( minf(player_mgheat, 100) * 680 / 100 ) + 100, 582, makecol(
			int( min(int(player_mgheat), 100) * 255 / 100 ), 128, 0
		));
		
		textout_ex(dbuf, font, "Hitpoints", 4, 586, makecol(128,0,0), -1);
		rectfill (dbuf, 100, 586, int( player_hitpoints * 680 / 100 ) + 100, 594, makecol(255,0,0));
		solid_mode();
		
		
		
		tower_angle = getangle(mouse_x, mouse_y, center_x, center_y);
		
		//textprintf_ex(dbuf, font, 2, 2, makecol(0, 192, 255), 0, "@: %f", tower_angle);
		
		// draw tower (just one for starters - and primitively)
		line (dbuf, center_x, center_y, (int)(center_x + 12 * sin(tower_angle)), (int)(center_y + 12 * cos(tower_angle)), makecol(255,0,0));
		circlefill (dbuf, WIDTH/2, HEIGHT/2, 8, makecol(128, 0, 0));
		
		// draw the buffer
		blit (dbuf, screen, 0, 0, 0, 0, WIDTH, HEIGHT);
		
		show_mouse (screen);
	}
	
	deinit();
	return 0;
}
END_OF_MAIN();


// simple hittest. needs improvement
bool hittest(int dotx, int doty, int bodyx, int bodyy, int bodyw, int bodyh) {
	if (dotx > bodyx && dotx < bodyx + bodyw
	&&  doty > bodyy && doty < bodyy + bodyh)
		return true;
	return false;
}


void init() {
	int res;
	allegro_init();
	// int depth = desktop_color_depth();
	// if (depth == 0) depth = 32;
	set_color_depth(32);
	request_refresh_rate(75);
	res = set_gfx_mode(GFX_AUTODETECT_WINDOWED, WIDTH, HEIGHT, 0, 0);
	if (res != 0) {
		allegro_message(allegro_error);
		exit(-1);
	}
	
	install_timer();
	install_keyboard();
	install_mouse();
	
	install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL);
}

void deinit() {
	clear_keybuf();

	destroy_bitmap(dbuf);
	//destroy_bitmap(temp_img);
	
	destroy_sample(snd_mg);
	destroy_sample(snd_ric);
	destroy_sample(snd_expl);
	
	int i;
	for (i = 0; i < 4; i++)
		destroy_sample(snd_secondary[i]);
	
	for (i = 0; i < 2; i++)
		destroy_bitmap(enemy_bitmaps[i]);
}
