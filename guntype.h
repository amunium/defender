class GunType {
public:
	GunType(int id, int speed, int recoil, int damage, int ammo, bool piercing, bool explosive) {
		this->id = id;
		this->speed = speed;
		this->recoil = recoil;
		this->damage = damage;
		this->piercing = piercing;
		this->explosive = explosive;
		this->counter = 0;
		this->ammo = ammo;
	}
	
	void recoil_reset() {
		this->counter = this->recoil;
	}
	
	int counter;
	int ammo;
	
	int id;
	int speed;
	int recoil;
	int damage;
	bool piercing;
	bool explosive;
};



GunType *guns[4];

void init_guns() {
	// id, speed, recoil, damage, ammo, piercing, explosive
	guns[0] = new GunType(0, 15, 4, 10, 0, false, false);		// machine gun - ammo is always 0
	guns[1] = new GunType(1, 20, 50, 100, 10, false, true);	// sabot
	guns[2] = new GunType(2, 5, 200, 500, 5, false, true);		// rocket
	guns[3] = new GunType(3, 50, 100, 1000, 3, true, false);		// railgun
}
