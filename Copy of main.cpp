#include <allegro.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <vector>
using namespace std;	// to avoid writing std::vector every time

#include "mathex.h"
#include "projectiles.h"
#include "guntype.h"
#include "enemies.h"

#define WIDTH		800
#define HEIGHT 		600
#define BASEHEIGHT	40
#define BASEWIDTH	25



void init();
void deinit();

volatile int fps = 0;



// bitmaps

BITMAP *dbuf;


// sounds

SAMPLE *snd_mg;


// vars

float tower_angle = 0;



// vector for all projectiles
vector<Bullet> v_bullets;

// vector for all enemies
vector<Enemy> v_enemies;



void frame_proc(void) {
	// decrement all recoil counters
	for (int i = 0; i < 2; i++) {
		if (guns[i]->counter > 0)
			guns[i]->counter--;
	}
}


int main() {
	init();
	
	LOCK_VARIABLE(fps);
	install_int_ex(frame_proc, BPS_TO_TIMER(50));
	
	
	init_guns();
	//init_enemies();
	
	
	dbuf = create_bitmap(WIDTH, HEIGHT);
	
	
	snd_mg = load_wav("data/sounds/mg.wav");
	
	
	// offsets for the base. no need to redraw every frame, unless it becomes variable later
	int baseoffset_x = (WIDTH-BASEWIDTH)/2;
	int baseoffset_y = (HEIGHT-BASEHEIGHT)/2;
	
	// offsets for tower
	int center_x = WIDTH/2;
	int center_y = HEIGHT/2;
	
	
	
	// main loop
	while (!key[KEY_ESC]) {
		vsync();
		clear_bitmap (dbuf);
		int i;	// for all level1 loops
		
		
		// fire machine gun
		if (mouse_b & 2 && guns[0]->counter <= 0) {
			guns[0]->recoil_reset();		// set back recoil counter
			
			// add a bullet to the vector
			v_bullets.insert(v_bullets.end(), 1, * new Bullet(center_x, center_y, guns[0]->speed, tower_angle, MG ));
			
			play_sample(snd_mg, 255, 0, 1000, 0);
		}
		// secondary weapon firing goes here
		
		
		/**
		 * DRAWING
		 */
		
		set_trans_blender(64, 192, 96, 128);
		// draw base (primitively for now)
		//rectfill (dbuf, (WIDTH-BASEWIDTH)/2, (HEIGHT-BASEHEIGHT)/2, WIDTH/2+BASEWIDTH/2, HEIGHT/2+BASEHEIGHT/2, makecol(64, 192, 96));
		rectfill (dbuf, baseoffset_x, baseoffset_y, baseoffset_x+BASEWIDTH, baseoffset_y+BASEHEIGHT, makecol(64, 192, 96));
		
		
		// draw fire
		for (i = 0; i < v_bullets.size(); i++) {
			
				// move the bullet according to angle and speed
				v_bullets.at(i).x += sin(v_bullets.at(i).angle) * v_bullets.at(i).speed;
				v_bullets.at(i).y += cos(v_bullets.at(i).angle) * v_bullets.at(i).speed;
				
				// if the bullet is offscreen, delete it
				if (v_bullets.at(i).x < -100 || v_bullets.at(i).x > WIDTH + 100
				|| v_bullets.at(i).y < -100 || v_bullets.at(i).y > HEIGHT + 100 ) {
					v_bullets.erase(v_bullets.begin() + i);
					continue;
				}
				
				// draw primitively for starters
				drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
				set_screen_blender(0, 0, 0, 64);
				
				// get length of projectile trail to no longer than distance to gun
				int length = min( v_bullets.at(i).speed * 10 , (int)sqrt(pow( v_bullets.at(i).x - center_x ,2) + pow( v_bullets.at(i).y - center_y ,2)) - 15 );
				
				// draw mg-bullet
				line (dbuf, (int)v_bullets.at(i).x, (int)v_bullets.at(i).y,
					(int)v_bullets.at(i).x - sin(v_bullets.at(i).angle) * length,
					(int)v_bullets.at(i).y - cos(v_bullets.at(i).angle) * length,
					makecol(255,255,255)
				);
				
				circlefill (dbuf, (int)v_bullets.at(i).x , (int)v_bullets.at(i).y, 1, makecol(255, 212, 128));
				solid_mode();
		}
		
		
		textprintf_ex(dbuf, font, 2, 2, makecol(0, 192, 255), 0, "len: %d", v_bullets.size());
		
		
		tower_angle = getangle(mouse_x, mouse_y, center_x, center_y);
		
		//textprintf_ex(dbuf, font, 2, 2, makecol(0, 192, 255), 0, "@: %f", tower_angle);
		
		// draw tower (just one for starters - and primitively)
		line (dbuf, center_x, center_y, (int)(center_x + 12 * sin(tower_angle)), (int)(center_y + 12 * cos(tower_angle)), makecol(255,0,0));
		circlefill (dbuf, WIDTH/2, HEIGHT/2, 8, makecol(128, 0, 0));
		
		// draw the buffer
		blit (dbuf, screen, 0, 0, 0, 0, WIDTH, HEIGHT);
		
		show_mouse (screen);
	}
	
	deinit();
	return 0;
}
END_OF_MAIN();


void init() {
	int res;
	allegro_init();
	// int depth = desktop_color_depth();
	// if (depth == 0) depth = 32;
	set_color_depth(32);
	request_refresh_rate(75);
	res = set_gfx_mode(GFX_AUTODETECT_WINDOWED, WIDTH, HEIGHT, 0, 0);
	if (res != 0) {
		allegro_message(allegro_error);
		exit(-1);
	}
	
	install_timer();
	install_keyboard();
	install_mouse();
	
	install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL);
}

void deinit() {
	clear_keybuf();

	destroy_bitmap(dbuf);
	
	destroy_sample(snd_mg);
	
	//for (int i = 0; i < v_bullets.size; i++) {
	//	
	//}
}
