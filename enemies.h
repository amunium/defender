// enemies.h
#define null 0

class EnemyType {
public:
	EnemyType(char *name, int hitpoints, int armor, int speed, int w_damage, int e_damage, int weapon, bool human, int imagenum) {
		this->name = name;
		this->hitpoints = hitpoints;
		this->armor = armor;
		this->speed = speed;
		this->w_damage = w_damage;
		this->e_damage = e_damage;
		this->weapon = weapon;
		this->human = human;
		this->imagenum = imagenum;
	}
	
	char *name;		
	int hitpoints;	
	int armor;		// all damage lowered by this amt - should mostly be 0
	int speed;		
	int w_damage;	// damage from mounted weapon
	int e_damage;	// damage on explosion
	int weapon;		// 0 = only explode on impact, 1 = machine gun, 2 = sabot, 3 = rocket
	bool human;
	int imagenum;
	
	int w;
	int h;	// for faster access
};

class Enemy {
public:
	Enemy(int x, int y, float angle, EnemyType &type) {
		this->x = x;
		this->y = y;
		this->angle = angle;
		this->hitpoints = type.hitpoints;
		this->type = &type;
	}
	
	float x;
	float y;
	float angle;
	int hitpoints;
	
	EnemyType *type;
};


EnemyType *enemytypes[2];

BITMAP *enemy_bitmaps[2];


// initialize enemies.. should probably load from file later
void init_enemies() {
	//						   name, hp, armor, speed, wdmg, edmg, weap, human, img
	enemytypes[0] = new EnemyType("Kamikaze", 100, 0, 2, null, 10, 0, true, 0);
	enemytypes[1] = new EnemyType("Jeep", 80, 0, 5, null, 10, 0, false, 1);
	
	enemy_bitmaps[0] = load_bitmap("data/graphics/jeep.bmp", NULL);
	enemy_bitmaps[1] = load_bitmap("data/graphics/jeep.bmp", NULL);
	
	for (int i = 0; i < 2; i++) {
		enemytypes[i]->w = enemy_bitmaps[enemytypes[i]->imagenum]->w;
		enemytypes[i]->h = enemy_bitmaps[enemytypes[i]->imagenum]->h;
	}
}
