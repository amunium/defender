// projectiles.h

//enum BulletTypes { MG = 0 , SABOT = 1 };

class Bullet {
public:
	Bullet(int x, int y, int speed, float angle, GunType &type) {
		this->x = (float)x;
		this->y = (float)y;
		this->speed = speed;;
		this->angle = angle;
		this->type = &type;
		this->disabled = false;
	}
	
	float x;
	float y;
	int speed;
	float angle;
	bool disabled;
	GunType *type;
};
