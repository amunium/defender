// mathex.h

//const double PI =	3.1415926535897932384626433832795;
const float PI = 3.14159;

float getangle(int, int, int, int);


float getangle (int target_x, int target_y, int center_x, int center_y) {
	float angle;
	
	if (target_x == center_x && target_y > center_y) {
		angle = PI/2;
	}
	else if (target_x > center_x && target_y > center_y) {
		angle = PI/2 - atan(((double)(target_x-center_x))/((double)(target_y-center_y)));
	}
	else if (center_x > target_x && target_y > center_y) {
		angle = PI/2 + atan(((double)(center_x-target_x))/((double)(target_y-center_y)));
	}
	else if (center_x > target_x && center_y > target_y) {
		angle = PI*1.5 - atan(((double)(center_x-target_x))/((double)(center_y-target_y)));
	}
	else {
		angle = PI*1.5 + atan(((double)(target_x-center_x))/((double)(center_y-target_y)));
	}
	
	angle = PI*.5 - angle;
	return angle;
}

// extended random function
int randex(int min, int max) {
	return (int)((rand() * (max - min)) / RAND_MAX) + min;
}
// float version of above
float randexf(float min, float max) {
	return (float)((rand() * (max - min)) / RAND_MAX) + min;
}

// Min/max
int min(int a, int b) {
	if (a < b)
		return a;
	else
		return b;
}
int max(int a, int b) {
	if (a > b)
		return a;
	else
		return b;
}

// Min/max float versions
float minf(float a, float b) {
	if (a < b)
		return a;
	else
		return b;
}
float maxf(float a, float b) {
	if (a > b)
		return a;
	else
		return b;
}
